#!/bin/bash

#
# - setup PAM locally with both a controller and KIE server
#

BASE_DIR=/vagrant

VB_PRIVATE_IP=$1
[[ -z "$VB_PRIVATE_IP" ]] && echo && echo && echo "ERROR: UNKNOWN VB_PRIVATE_IP - SET IT IN Vagrantfile --- ABORTING" && echo && echo && exit 10

command -v java     &> /dev/null || { echo >&2 'ERROR: JAVA not installed. Please install JAVA.8 to continue - Aborting'; exit 1; }
command -v mvn      &> /dev/null || { echo >&2 'ERROR: Maven not installed. Please install Maven.3.3.9 (or later) to continue - Aborting'; exit 1; }
command -v git      &> /dev/null || { echo >&2 'ERROR: GIT not installed. Please install GIT.1.8 (or later) to continue - Aborting'; exit 1; }
command -v unzip    &> /dev/null || { echo >&2 'ERROR: UNZIP not installed. Please install UNZIP to continue - Aborting'; exit 1; }
command -v curl     &> /dev/null || { echo >&2 'ERROR: CURL not installed. Please install CURL to continue - Aborting'; exit 1; }
command -v basename &> /dev/null || { echo >&2 'ERROR: basename not installed. Please install basename to continue - Aborting'; exit 1; }
command -v sed      &> /dev/null || { echo >&2 'ERROR: sed not installed. Please install sed to continue - Aborting'; exit 1; }

for f in $BASE_DIR/*; do
  [[ ! -r `basename $f` ]] && ln -s $f
done
rm -f simple-rule
cp -r $BASE_DIR/simple-rule .
rm -f settings.xml
cp $BASE_DIR/settings.xml .
$BASE_DIR/pam-setup.sh -b multi=4 -n $VB_PRIVATE_IP:8080 -o node1_config=standalone_eap.config:node2_config=node2_eap.config:node3_config=node3_eap.config:node4_config=node4_eap.config

source /root/nexus.pass

#
# modifying settings.xml with the nexus password obtained
#
pushd jboss-eap-7.2 &> /dev/null
  sed -i "s/@@NEXUS_PASSWORD@@/${NEXUS_ROOT_PASS}/g" settings.xml
  sed -i "s/@@NEXUS_IP@@/${VB_PRIVATE_IP}/g" settings.xml
  cp settings.xml $BASE_DIR/settings.xml.nexus
popd &> /dev/null

#
# systemd integration
#
$BASE_DIR/pam7-systemd-integration.sh

#
# build simple rule project
#
pushd simple-rule &> /dev/null
  echo "Building simple-rule project..."
  mvn clean deploy -DskipTests
  mvn clean deploy -DskipTests
  ./update-australia.js
  ./update-brazil.js
popd &> /dev/null
