#!/bin/bash

#
# - setup a local environment with BusinessCentral and one KIE Server 
#   installed in a single EAP node
#

#
# === ENVIRONMENT DIVISION - CONFIGURATION SECTION ===
#

#
# - EAP/PAM setup configuration variables
#
#                     eapAdminName/eapAdminPasswd : credentials for EAP administrator
#
#                     pamAdminName/pamAdminPasswd : credentials for PAM administrator
#
#           kieServerUserName/kieServerUserPasswd : credentials for the user the controller
#                                                   should use to connect to the KIE Execution Server
#                                                   corresponds to org.kie.server.user/.pwd settings
#
#   kieControllerUserName/kieControllerUserPasswd : credentials to connect to the controller REST API
#                                                   corresponds to org.kie.server.controller.user/.pwd settings
#
#                                        serverId : leave blank to autogenerate value
#                                                   unique value for a cluster would have indepedent KIE ES
#                                                   use same value to control all KIE ES
#
eapAdminName='admin'
eapAdminPasswd='S3cr3tK3y#'

pamAdminName='pamAdmin'
pamAdminPasswd='S3cr3tK3y#'

kieServerUserName='kieServerUser'
kieServerUserPasswd='kieServerUser1234;'

kieControllerUserName='controllerUser'
kieControllerUserPasswd='controllerUser1234;'

serverId='remote-kieserver'

#
# === END OF CONFIGURATION SECTION
#
# -- No need to configure anything beyond this point
#

#
# check environment
#
command -v java &> /dev/null || { echo >&2 'ERROR: JAVA not installed. Please install JAVA.8 to continue - Aborting'; exit 1; }
#command -v mvn &> /dev/null || { echo >&2 'ERROR: Maven not installed. Please install Maven.3.3.9 (or later) to continue - Aborting'; exit 1; }
#command -v git &> /dev/null || { echo >&2 'ERROR: GIT not installed. Please install GIT.1.8 (or later) to continue - Aborting'; exit 1; }
command -v unzip &> /dev/null || { echo >&2 'ERROR: UNZIP not installed. Please install UNZIP to continue - Aborting'; exit 1; }
command -v curl &> /dev/null || { echo >&2 'ERROR: CURL not installed. Please install CURL to continue - Aborting'; exit 1; }
command -v sqlite3 &> /dev/null || { echo >&2 'ERROR: SQLite not installed. Please install SQLite to continue - Aborting'; exit 1; }
command -v hostid &> /dev/null || { echo >&2 'ERROR: SQLite not installed. Please install SQLite to continue - Aborting'; exit 1; }

CONFIG_DB="$PWD"/pam-config.db

# check if stdout is a terminal...
if test -t 1; then

    # see if it supports colors...
    ncolors=$(tput colors)

    if test -n "$ncolors" && test $ncolors -ge 8; then
        bold="$(tput bold)"
        underline="$(tput smul)"
        standout="$(tput smso)"
        normal="$(tput sgr0)"
        black="$(tput setaf 0)"
        red="$(tput setaf 1)"
        green="$(tput setaf 2)"
        yellow="$(tput setaf 3)"
        blue="$(tput setaf 4)"
        magenta="$(tput setaf 5)"
        cyan="$(tput setaf 6)"
        white="$(tput setaf 7)"
    fi
fi

MASTER_CONFIG=master.conf

#
# versions supported
#
cat << "__CONFIG" > $MASTER_CONFIG
PAM760 | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.6.0-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.6.0-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
PAM751 | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.5.1-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.5.1-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
PAM75  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.5.0-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.5.0-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
DM741  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhdm-7.4.1-decision-central-eap7-deployable.zip  | KIE_ZIP=rhdm-7.4.1-kie-server-ee8.zip  | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=DM
PAM741 | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.4.1-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.4.1-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
PAM74  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.4.0-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.4.0-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
PAM731 | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=jboss-eap-7.2.*-patch.zip | PAM_ZIP=rhpam-7.3.1-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.3.1-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
DM731  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=                          | PAM_ZIP=rhdm-7.3.1-decision-central-eap7-deployable.zip  | KIE_ZIP=rhdm-7.3.1-kie-server-ee8.zip  | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=DM
PAM73  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=                          | PAM_ZIP=rhpam-7.3.0-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.3.0-kie-server-ee8.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
PAM72  | EAP7_ZIP=jboss-eap-7.2.0.zip | EAP_PATCH_ZIP=                          | PAM_ZIP=rhpam-7.2.0-business-central-eap7-deployable.zip | KIE_ZIP=rhpam-7.2.0-kie-server-ee7.zip | PAM_PATCH_ZIP= | INSTALL_DIR=jboss-eap-7.2 | TARGET_TYPE=PAM
__CONFIG

#
# define helper functions
#
function extractHeaders() {
  grep -v '^#' $MASTER_CONFIG | awk -F'|' '{ if (NF>0) printf "\t"$1"\n"; }'
}

function extractConfiguration() {
  grep -v '^#' $MASTER_CONFIG | grep "^$2 " | awk -F'|' '{ if (NF>0) for (i=2; i<=NF; i++) printf $i"\n"; }' > $1
  cp $1 /tmp/$2.conf
}

function randomid() {
  # longer version, seems overkill
  #echo `od -x /dev/urandom | head -1 | awk '{OFS="-"; print $2$3,$4,$5,$6,$7$8$9}'`
  echo `od -x /dev/urandom | head -1 | awk '{OFS="-"; print $2$3$4}'`
}

function bigString() {
  local in=$1
  local le=${#in}
  local o=$in;
  if [ $le -gt 60 ]; then
    o=${in:0:15}'...'${in: -25}
  fi
  echo $o
}

function sout() {
  declare -a arr=("$@")
  for i in "${arr[@]}"; do
    echo ':: '$i
  done
}

function waitForKey() {
  echo '... :::         ::: ...'
  echo '... ::: WAITING ::: ...'
  echo '... :::         ::: ...'
  echo
  echo "$@"
  echo
  read -p 'Press ENTER key to continue...'
  echo
}

declare -a summaryAr
function summary() {
  declare -a arr=("$@")
  summaryAr=("${summaryAr[@]}" "${arr[@]}")
}

function prettyPrinter() {
  declare -a arr=("$@")
  local maxlen=0
  for i in "${arr[@]}"; do
    local h=`echo $i | grep -v '^-' | awk -F':-' '{ if (NF>0) printf $1; }'`
    local strlenh=${#h}
    [[ $strlenh -gt $maxlen ]] && maxlen=$strlenh
  done
  local spacer=`printf '%*s' $maxlen`
  for i in "${arr[@]}"; do
    local h=`echo $i | grep -v '^-' | awk -F':-' '{ if (NF>0) printf $1; }'`
    local strlen=${#h}
    h=" ${spacer}${h}"
    local r=`echo $i | awk -F':-' '{ if (NF>0) for (i=2; i<=NF; i++) printf $i; }'`
    local rprefix=""
    local rsuffix=""
    [[ "$r" == "" ]] && r=$i && rprefix="${bold}${white}"
    [[ "$r" != "$i" ]] && echo -n " ${h: -$maxlen}:" && rprefix=" [${bold}${blue}" && rsuffix=" ]"
    echo "${rprefix}${r}${normal}${rsuffix}"
  done
}


#
# echo the value for the config key escaping slashes
# value specified in pam.config file for key overrides supplied value
#
# key is either "key" or "COMMENT:key"
#
function prepareConfigLine() {
  local key="$1"
  local val="$2"
  local tmpf="$3"
  local result="$val"
  local args=() && while read -rd:; do args+=("$REPLY"); done <<<"$key:" && local keyPrefix="${args[0]}" && key="${args[1]}"
  [[ -z $key ]] && key="$keyPrefix"
  if [[ "x$key" != "xCOMMENT" ]]; then
    [[ ! -z "$key" ]] && [ ${pamConfigAr[$key]+xxx} ] && result=${pamConfigAr[$key]}
    # result=$(echo ${result} | sed -e "s#/#\\\/#g")
    key="'""$key""'"
    result="$(printf "<property name=%-40s value='%s'/>" "$key" "$result")"
    # no result for previous key
    [[ ${pamConfigOutAr[$key]+xxx} ]] && result=''
    pamConfigOutAr[$key]="value"
  fi
  [[ "x$keyPrefix" == "xCOMMENT" ]] && result='<!-- '"$result"' -->'
  [[ ! -z $result ]] && echo "<!-- UNIQ_MARK_1 -->""$result" >> $tmpf
}

function usage() {
echo "
Will install PAM on a standalone EAP node or an EAP cluster. Execute this script on each
node that will be part of the cluster.

usage: `basename $0` [-h help]
                     -n ip[:node]
                     -b [kie|controller|both|multi=2...], defaults to 'both'
                     [-c ip1:port1,ip2:port2,...]
                     [-s smart_router_ip:port]
                     [-o additional_options ], specify additional options

example: `basename $0` -n localhost

Options:
    -n : the IP[:port] of the node it will operate on, default is localhost:8080
    
    -b : Configure PAM installation
             kie : only the KIE ES component
      controller : only the Business Central component without the KIE ES
            both : (default) full suite of PAM along with a separate KIE ES having
                   same node as controller, suitable for a development environment
                   
           multi : a multi-node managed KIE Server installation, with the following
                   configuration:
                   node 1 : 'both' mode installation, i.e. Business Central and KIE ES
                   node 2 : 'kie' mode installation, i.e. only the KIE ES
                   node 3 onwards: 'kie' mode installation
                   
                   Startup scripts will be generated according to the number of nodes specified
                   Node 1 will use the IP:PORT specified with '-n' with each subsequent node
                   using a port offset of 100
                   
    -c :  Manadatory for 'kie' mode of PAM installation, ignored in other modes
          Specify list of controllers that this KIE ES should connect to.
          List of controllers in the form of comma-sperated list of 'IP:PORT' pairs
          e.g. 10.10.1.20:8080,192.168.1.34:8350
          
     -s : Only for KIE ES, optional. Specify Smart Router location, eg 10.10.1.23:9000
     
     -o : Specify additional options. Supported options are:
     
          - nodeX_config=file : declare file with additional commands to be applied by
                                EAPs jboss-cli tool for each node installed
                                X stands for the number of each node, e.g. node1_config, node2_config, etc
                                
          - debug_logging     : if present will set logging level to DEBUG
          
          - dump_requests     : if present will enable request dumping to log file
          
                                WARNING: enabling debug_logging and dump_requests
                                can generate copious amount of output and can have
                                significant impact on perforance
     
     -h : print this message
     
Notes:
  - If a file named settings.xml is found in the current directory during
    installation it will be set as custom maven settings through the use of
    kie.maven.settings.custom system property.
    The file will be copied to EAP_HOME and the system property will point to
    that.
    
  - The following users will be added as part of the installation:

      User            Role
      ----            ----
      admin           EAP admin
      pamAdmin        rest-all,kie-server,admin,analyst,kiemgmt
      pamAnalyst      analyst
      pamDeveloper    developer
      pamUser         user
      kieServerUser   kie-server,rest-all
      controllerUser  kie-server,rest-all

    Passwords for these users will be printed at the end of the installation in stdout      
"
}

#
# - install Users - same users for all nodes
#
function installUsers() {
  local nodedir=${1:-standalone}
  # have to add EAP admin user as PAM overrides config
  local scPath="$EAP_HOME/${nodedir}/configuration"
  [[ "$CYGWIN_ON" == "yes" ]] && scPath=$(cygpath -w ${scPath})
  pushd $EAP_HOME/bin &> /dev/null
    ./add-user.sh -sc $scPath -s --user "$eapAdminName" --password "$eapAdminPasswd"
    ./add-user.sh -sc $scPath -s -a --user "$pamAdminName" --password "$pamAdminPasswd" --role kie-server,rest-all,admin,analyst,kiemgmt,manager,user
    ./add-user.sh -sc $scPath -s -a --user "$kieServerUserName" --password "$kieServerUserPasswd" --role kie-server,rest-all
    ./add-user.sh -sc $scPath -s -a --user "$kieControllerUserName" --password "$kieControllerUserPasswd" --role kie-server,rest-all
    summary "Added EAP admin user :- $eapAdminName / $eapAdminPasswd" 
    summary "Added PAM admin user :- $pamAdminName / $pamAdminPasswd" 
    summary "Added KIE Server user :- $kieServerUserName / $kieServerUserPasswd" 
    summary "Added Conntroller user :- $kieControllerUserName / $kieControllerUserPasswd" 
    # 
    # add additional users 
    #
    extraPasswd="r3dh4t456^"
    extraUser="pamAnalyst"   && ./add-user.sh -sc $scPath -s -a --user "$extraUser" --password "$extraPasswd" --role rest-all,analyst
    extraUser="pamDeveloper" && ./add-user.sh -sc $scPath -s -a --user "$extraUser" --password "$extraPasswd" --role rest-all,developer
    extraUser="pamUser"      && ./add-user.sh -sc $scPath -s -a --user "$extraUser" --password "$extraPasswd" --role rest-all,user
    summary "Added PAM analyst   user :- pamAnalyst   / $extraPasswd" 
    summary "Added PAM developer user :- pamDeveloper / $extraPasswd" 
    summary "Added PAM user      user :- pamUser      / $extraPasswd" 
  popd &> /dev/null
}

#
# - install BC
#
function installBC() {
  cd $cwd
  unzip -qq -o $PAM_ZIP
  summary "Installed PAM using :- `bigString $PAM_ZIP`"
}

#
# - install KIE Server
#
function installKIE() {
  local nodedir=${1:-standalone}
  cd $cwd
  rm -rf tmp/kie_zip
  mkdir -p tmp/kie_zip
  cp $KIE_ZIP tmp/kie_zip
  pushd tmp/kie_zip &> /dev/null
    unzip -qq -o $KIE_ZIP
  popd &> /dev/null
  cp -r tmp/kie_zip/kie-server.war $EAP_HOME/${nodedir}/deployments
  : > $EAP_HOME/${nodedir}/deployments/kie-server.war.dodeploy
  cp tmp/kie_zip/SecurityPolicy/* $EAP_HOME/bin
  rm -rf tmp
  summary "Installed KIE SERVER :- `bigString $KIE_ZIP`" 
}


#
# - check configuration - PAM needs full, clustering best with HA
#
function checkConfiguration() {
  local nodedir=${1:-standalone}
  cd $cwd
  xmlConfig=$EAP_HOME/${nodedir}/configuration/standalone.xml
  xmlConfigHA=$EAP_HOME/${nodedir}/configuration/standalone-full-ha.xml
  if [ ! -r $xmlConfig ]; then
    sout "ERROR: Cannot read configuration $xmlConfig -- exiting"
    exit 1;
  fi
  if [ ! -r $xmlConfigHA ]; then
    sout "ERROR: Cannot read configuration $xmlConfigHA -- exiting"
    exit 1;
  fi
  cp $xmlConfig $xmlConfig-orig
  cp $xmlConfigHA $xmlConfig
  #
  # custom directories to accommodate multinode installation
  #
  mkdir -p $EAP_HOME/${nodedir}/{kie,git,metaindex}
}

#
# -- modify $xmlConfig with defaults for PAM setup and EAP cluster setup
#
#    more on system properties at:
#    https://access.redhat.com/documentation/en-us/red_hat_process_automation_manager/7.3/html/installing_and_configuring_red_hat_process_automation_manager_on_red_hat_jboss_eap_7.2/run-dc-standalone-proc#run-standalone-properties-con
#
# sample:
#   enter the password to be stored:
#   echo "foufoutos" | keytool -importpassword -keystore skye.jceks -keypass passwdForKey -alias keyOne -storepass keystorePass -storetype JCEKS
#
function modifyConfiguration() {
  local nodedir=${1:-standalone}
  #local nodeCounter=${2:-0}
  #nodeCounter=$((nodeCounter-1)) && [[ "$nodedir" == "standalone" ]] && nodeCounter=0
  #local nodeOffset=${nodeConfig['nodeOffset']}
  #nodePort=$((basePort+nodeOffset))
  checkConfiguration $nodedir
  BASE_URL='http://'${nodeIP}:${nodeConfig['nodePort']}

  #
  # - if pam.config found, take it into account
  #
  #   comments support : https://stackoverflow.com/a/48155918
  #
  declare -A pamConfigOutAr
  declare -A pamConfigAr
  if [[ -r pam.config ]]; then
    while read PARAM VALUE; do
     [[ "$PARAM" =~ ^[[:space:]]*# ]] && continue
     if [[ ! -z $PARAM ]] && [[ ! -z $VALUE ]]; then
       pamConfigAr[$PARAM]="$VALUE"
     fi
    done < pam.config
  fi

  #
  # - if run again avoid modifying configuration if already touched
  #
  if [ `grep UNIQ_MARK_1 $xmlConfig | wc -l` -eq 0 ]; then
    #
    # - create keystore with the KIE server and Controller user
    #
    local ksPath="$EAP_HOME/${nodedir}/configuration"
    [[ "$CYGWIN_ON" == "yes" ]] && ksPath=$(cygpath -w ${ksPath})
    echo "$kieServerUserPasswd"     | keytool -noprompt -importpassword -keystore "$ksPath/eigg.jceks" -keypass kieServerUserPasswd     -alias kieServerUser     -storepass eiggPass -storetype JCEKS 2> /dev/null
    echo "$kieControllerUserPasswd" | keytool -noprompt -importpassword -keystore "$ksPath/eigg.jceks" -keypass kieControllerUserPasswd -alias kieControllerUser -storepass eiggPass -storetype JCEKS 2> /dev/null

    # - generate random serverId if none specified
    if [ "$serverId" == "" ]; then
      serverId='r-'`randomid`
    fi
    tmpf=tmp.`randomid`
    : > $tmpf
    # https://access.redhat.com/solutions/721613
    prepareConfigLine "COMMENT"                               'following property no required for PAM, useful for clustered deployemnts, harmless otherwise'  $tmpf
    prepareConfigLine "jboss.node.name"                       "${nodeConfig[nodeName]}"  $tmpf
    prepareConfigLine "jboss.tx.node.id"                      "${nodeConfig[nodeName]}"  $tmpf
    prepareConfigLine "org.kie.server.repo"                   '${jboss.server.data.dir}'  $tmpf
    prepareConfigLine "org.uberfire.nio.git.dir"              '${jboss.server.base.dir}/git'  $tmpf
    prepareConfigLine "org.uberfire.metadata.index.dir"       '${jboss.server.base.dir}/metaindex'  $tmpf
    prepareConfigLine "org.guvnor.m2repo.dir"                 '${jboss.server.base.dir}/kie'  $tmpf
    prepareConfigLine "org.guvnor.project.gav.check.disabled" 'true'  $tmpf
    prepareConfigLine "COMMENT"                               '<property name="org.kie.server.domain" value="user_authntication_JAAS_LoginContext_domain_when_using_JMS"/>'  $tmpf
    # check for settings.xml and (un)comment accordingly while copying settings.xml in place
    local keyPrefix="COMMENT" && [[ -r settings.xml ]] && cp settings.xml $EAP_HOME && keyPrefix=""
    # cPrefix='<!-- ' && cSuffix=' -->' && [[ -r settings.xml ]] && cPrefix="" && cSuffix="" 
    prepareConfigLine "COMMENT"                                    'set kie.maven.settings.custom to your custom settings.xml'  $tmpf
    prepareConfigLine "${keyPrefix}:kie.maven.settings.custom"     '${jboss.home.dir}/settings.xml'  $tmpf
    prepareConfigLine "COMMENT:org.kie.server.controller.connect"  '10000_milliseconds_delay_for_controller_connect'  $tmpf
    prepareConfigLine "appformer.ssh.keys.storage.folder"          '${jboss.server.base.dir}/ssh_keys'  $tmpf
    prepareConfigLine "COMMENT"                                    'uncomment following line to enable git hooks'  $tmpf
    prepareConfigLine "COMMENT:org.uberfire.nio.git.hooks"         '${jboss.home.dir}/git-hooks-source'  $tmpf
    # prepareConfigLine "org.uberfire.nio.git.hooks"                 '${jboss.home.dir}/git-hooks-source'  $tmpf
    prepareConfigLine "appformer.git.hooks.bundle"                 '${jboss.home.dir}/git-hooks-messages/Messages.properties'  $tmpf
    prepareConfigLine "COMMENT"                                    '.........'  $tmpf
    prepareConfigLine "COMMENT"                                    'properties for Unified KIE Server setup'  $tmpf
    prepareConfigLine "COMMENT"                                    'the following values must be the same on all of the KIE Execution Servers'  $tmpf
    prepareConfigLine "org.kie.server.persistence.ds"              'java:jboss/datasources/ExampleDS'  $tmpf
    prepareConfigLine "org.kie.server.persistence.dialect"         'org.hibernate.dialect.H2Dialect'  $tmpf
    prepareConfigLine "org.kie.executor.jms.queue"                 'queue/KIE.SERVER.EXECUTOR'  $tmpf
    prepareConfigLine "COMMENT"                                    '.........'  $tmpf
    prepareConfigLine "kie.keystore.keyStoreURL"                   'file:///${jboss.server.config.dir}/eigg.jceks'  $tmpf
    prepareConfigLine "kie.keystore.keyStorePwd"                   'eiggPass'  $tmpf

    if [ "$pamInstall" != "kie" ]; then
      prepareConfigLine "COMMENT"                                    'properties for controller/business-central in a managed KIE Server setup'  $tmpf
      prepareConfigLine "org.kie.server.user"                        "$kieServerUserName"  $tmpf
      #
      # following is deprecated as of RHPAM.7.4
      # echo "$(prepareConfigLine "org.kie.server.pwd"             "$kieServerUserPasswd"  $tmpf
      #
      prepareConfigLine "kie.keystore.key.server.alias"              'kieServerUser'  $tmpf
      prepareConfigLine "kie.keystore.key.server.pwd"                'kieServerUserPasswd'  $tmpf
      prepareConfigLine "COMMENT"                                    '.........'  $tmpf
    fi
 
    if [ "$pamInstall" != "controller" ]; then
      # - build clv based on controllerListAr
      clv=''
      for i in ${controllerListAr[@]}; do
        local baseController=business-central
        [[ "$TARGET_TYPE" == "DM" ]] && baseController=decision-central
        clv="$clv,${i}/$baseController/rest/controller"
      done
      clv=${clv#,}
      nodeConfig['controllerUrl']="${clv}"
      prepareConfigLine "COMMENT"                                    'KIE server capabilities'  $tmpf
      prepareConfigLine "org.drools.server.ext.disabled"             'false'  $tmpf
      if [[ "$TARGET_TYPE" != "DM" ]]; then
        prepareConfigLine "org.jbpm.server.ext.disabled"             'false'  $tmpf
        prepareConfigLine "org.jbpm.ui.server.ext.disabled"          'false'  $tmpf
      fi
      prepareConfigLine "org.optaplanner.server.ext.disabled"        'false'  $tmpf
      prepareConfigLine "COMMENT"                                    '.........'  $tmpf
      if [ "$pamInstall" == "kie" ] || [ "$pamInstall" == "both" ]; then
        #local sedclv=$(echo ${clv} | sed -e "s#/#\\\/#g")
        prepareConfigLine "COMMENT"                                  'properties for managed KIE Server'  $tmpf
        prepareConfigLine "org.kie.server.id"                        "$serverId"  $tmpf
        prepareConfigLine "org.kie.server.location"                  "$BASE_URL/kie-server/services/rest/server"  $tmpf
        prepareConfigLine "org.kie.server.controller"                "${clv}"  $tmpf
        prepareConfigLine "org.kie.server.controller.user"           "$kieControllerUserName"  $tmpf
        prepareConfigLine "COMMENT"                                  'overcome 256 character limitation in process variable values'  $tmpf
        prepareConfigLine "COMMENT"                                  'if you uncomment the following parameter, remember to alter your database column accordingly'  $tmpf
        prepareConfigLine "COMMENT"                                  'see also: https://issues.redhat.com/browse/JBPM-4221'  $tmpf
        prepareConfigLine "COMMENT:org.jbpm.var.log.length"          "1000"                    $tmpf
        #
        # deprecated after RHPAM.7.4
        # echo '<!-- UNIQ_MARK_1 --><property name="org.kie.server.controller.pwd"  value="REPLACE_ME"/>' | sed s/REPLACE_ME/"$kieControllerUserPasswd"/g >> $tmpf
        #
        prepareConfigLine "kie.keystore.key.ctrl.alias"              "kieControllerUser"  $tmpf
        prepareConfigLine "kie.keystore.key.ctrl.pwd"                "kieControllerUserPasswd"  $tmpf
        if [[ ! -z $smartRouter ]]; then                                                                                                     
          prepareConfigLine "org.kie.server.router"                  "http://${smartRouter}"  $tmpf
        fi                                                                                                                                   
        prepareConfigLine "COMMENT"                                  'set following to false to enable Prometheus end points'  $tmpf
        prepareConfigLine "org.kie.prometheus.server.ext.disabled"   "true"  $tmpf
        prepareConfigLine "COMMENT"                                  'provide custom prediction service'  $tmpf
        prepareConfigLine "COMMENT:org.jbpm.task.prediction.service" 'SMILERandomForest'  $tmpf
        prepareConfigLine "COMMENT"                                  '.........'  $tmpf
      fi                                                                                                                                     
    fi                                                                                                                                       
                                                                                                                                             
    prepareConfigLine "COMMENT"                                      'properties for controller/Business-Central'  $tmpf
    prepareConfigLine "COMMENT"                                      'default is full profile'  $tmpf
    if [ "$pamInstall" == "controller" ]; then                                                                                               
      prepareConfigLine "COMMENT:org.kie.active.profile"                     'full'  $tmpf
    else                                                                                                                                     
      prepareConfigLine "COMMENT:org.kie.active.profile"                     'one_of_full|exec-server|ui-server'  $tmpf
    fi
  
    #
    # - if pam.config found, include all variables in the output
    #
    if [[ -r pam.config ]]; then
      prepareConfigLine "COMMENT"           'configuration properties found at pam.config'  $tmpf
      for key in "${!pamConfigAr[@]}"; do
         prepareConfigLine "${key}"         "${pamConfigAr[$key]}"  $tmpf
         #echo '<!-- UNIQ_MARK_1 --><property name="'${key}'" value="'${pamConfigAr[$key]}'"/>  ' >> $tmpf
       done
    fi

    # - try to pinpoint where system-properties start or should start
    propPrefix="<system-properties>"
    propSuffix="</system-properties>"
    line=`grep -H -n system-properties $xmlConfig | tail -1 | cut -d':' -f2`
    if [[ "$line" != "" ]]; then
      line1=$((line-1))
      propPrefix=" "
      propSuffix=" "
    else
      line=`grep -H -n extensions $xmlConfig | tail -1 | cut -d':' -f2`
      line1=$((line))
    fi
    echo "$propPrefix" > $tmpf.props
    cat $tmpf >> $tmpf.props
    echo "$propSuffix" >> $tmpf.props
    mv $tmpf.props $tmpf
    sed "${line1}r $tmpf" $xmlConfig > $tmpf.1
    mv $tmpf.1 $xmlConfig
    rm -f $tmpf*
    # try to safeguard exposed interfaces
    l=`grep -H -n '<interface name="management">' $xmlConfig | head -1 | cut -d':' -f2`;
    #  modify management interface, should already be 127.0.0.1 but making sure
    let lno=$((l+1))
    sed -i "${lno}s/:.*}/:127.0.0.1}/" $xmlConfig
    sync
    # - modify public interface
    let lno=$((l+4))
    sed -i "${lno}s/:.*}/:$nodeIP}/" $xmlConfig
    sync
    # - modify private interface
    let lno=$((l+7))
    sed -i "${lno}s/:.*}/:$nodeIP}/" $xmlConfig
    sync
    rm -f $tmpf*
    
    #
    # modify logging to output to console as well as file
    #
    tmpf=tmp.`randomid`
    : > $tmpf
    l=`grep -H -n 'periodic-rotating-file-handler' $xmlConfig | head -1 | cut -d':' -f2`;
    echo '<!-- NEW_LOG_HANDLER --> <console-handler name="CONSOLE">' >> $tmpf
    echo '<!-- NEW_LOG_HANDLER -->     <level name="INFO"/>' >> $tmpf
    echo '<!-- NEW_LOG_HANDLER -->     <formatter>' >> $tmpf
    echo '<!-- NEW_LOG_HANDLER -->         <named-formatter name="COLOR-PATTERN"/>' >> $tmpf
    echo '<!-- NEW_LOG_HANDLER -->     </formatter>' >> $tmpf
    echo '<!-- NEW_LOG_HANDLER --> </console-handler>' >> $tmpf
    let lno=$((l-1))
    sed -i -e "${lno}r $tmpf" $xmlConfig
    : > $tmpf
    l1=`grep -H -n '<root-logger>' $xmlConfig | head -1 | cut -d':' -f2`;
    let l2=$((l1+2))
    echo '<!-- NEW_LOG_HANDLER --> <handler name="CONSOLE"/>' > $tmpf
    sed -i -e "${l2}r $tmpf" $xmlConfig
    rm -f $tmpf*
  fi
}

#
# - create a sample start up script
#
function startUp() {
  local nodedir=${1:-standalone}
  local nodeCounter=${2:-0}
  # nodeCounter=$((nodeCounter-1)) && [[ "$nodedir" == "standalone" ]] && nodeCounter=0
  # local nodeOffset=$((nodeCounter*100))
  # local nodePort=$((basePort+nodeOffset))
  # nodeOffset=$((nodePort-8080))
  local nodeOffset=${nodeConfig['nodeOffset']}
  local startScript=gopam.sh
  [[ "$nodeCounter" -gt 0 ]] && startScript=go${nodedir}.sh
  nodeConfig['startScript']="$startScript"
  cat << __GOPAM > $startScript
  #!/bin/bash
  
  #
  # usage: ./gopam.sh configuration-xml IP-to-bind-to port-offset
  #
  # example: ./${startScript}
  #          by default will start on localhost with standalone.xml and default ports (8080)
  #
  #          ./${startScript} standalone.xml 0.0.0.0 100
  #          will start with standalone.xml binding on all IPs and on port 8180 (port offset 100)
  
  JBOSS_CONFIG=\${1:-standalone.xml}
  JBOSS_BIND=\${2:-localhost}
  JBOSS_PORT_OFFSET=\${3:-$nodeOffset}
  [[ "\$JBOSS_PORT_OFFSET" != "0" ]] && JBOSS_PORT_OFFSET="-Djboss.socket.binding.port-offset=\$JBOSS_PORT_OFFSET"
  [[ "\$JBOSS_PORT_OFFSET" == "0" ]] && JBOSS_PORT_OFFSET=" "
  [[ -z "\$JBOSS_HOME" ]] && JBOSS_HOME=$EAP_HOME
  CLI_OPTIONS=" -Djava.security.egd=file:/dev/./urandom "
  if [[ \$(uname | grep -i CYGWIN) ]]; then
    JBOSS_HOME=\$(cygpath -w \${JBOSS_HOME})
    CLI_OPTIONS=" "
  fi

  pushd \${JBOSS_HOME}/bin/ &> /dev/null
    ./standalone.sh -b \$JBOSS_BIND -c \$JBOSS_CONFIG \$JBOSS_PORT_OFFSET -Djboss.server.base.dir=\$JBOSS_HOME/$nodedir \$CLI_OPTIONS
  popd &> /dev/null
__GOPAM
  chmod u+x $startScript
  summary "Startup script :- $startScript"
}

#
# - enable HTTP request dumping and access log
#
function applyAdditionalNodeConfig() {
  local nodedir=${1:-standalone}
  local tmpfile=out.`randomid`
  local pamAdmPort=$((9990+${nodeConfig[nodeOffset]}))
  local startScript=${nodeConfig[startScript]}
  local k=${nodeConfig[nodeCounter]}_config
  local nodeOffset=${nodeConfig['nodeOffset']}
  #
  # apply node offset: /socket-binding-group=standard-sockets/:write-attribute(name=port-offset,value=${jboss.socket.binding.port-offset:100})
  #
  # apply any node specific configuration
  #
  [[ ! -r "${configOptions[$k]}" ]] && [[ -z "${configOptions[debug_logging]}" ]] && [[ -z "${configOptions[dump_requests]}" ]] && return
  #
  ./$startScript &> $tmpfile &
  #
  echo -ne "Waiting for ${nodeConfig[nodeName]} to start..."
  local goon=no; while [[ "$goon" == "no" ]]; do sleep 2s; curl -s http://localhost:${nodeConfig[nodePort]} -o /dev/null; r=$?; [[ $r -eq 0 ]] && goon=yes; echo -ne '.' ; done
  echo ' DONE'
  eapFile=eapFile_`randomid` && : > $eapFile
  if [[ "$nodeOffset" != 0 ]]; then
    echo "/socket-binding-group=standard-sockets/:write-attribute(name=port-offset,value=\${jboss.socket.binding.port-offset:$nodeOffset})" >> $eapFile
  fi
  $EAP_HOME/bin/jboss-cli.sh --controller=localhost:$pamAdmPort --connect --file=$eapFile --output-json
  rm -f $eapFile
  if [[ -r "${configOptions[$k]}" ]]; then
    sout "Applying additional configuration for ${nodeConfig[nodeCounter]} based on ${configOptions[$k]}"
    $EAP_HOME/bin/jboss-cli.sh --controller=localhost:$pamAdmPort --connect --file=${configOptions[$k]} --output-json
  fi
  if [[ ! -z "${configOptions[dump_requests]}" ]]; then
    $EAP_HOME/bin/jboss-cli.sh --controller=localhost:$pamAdmPort --connect --commands='/subsystem=undertow/configuration=filter/expression-filter=requestDumperExpression:add(expression="dump-request")'
    $EAP_HOME/bin/jboss-cli.sh --controller=localhost:$pamAdmPort --connect --commands="/subsystem=undertow/server=default-server/host=default-host/filter-ref=requestDumperExpression:add"
  fi
  echo -ne "Shutting down ${nodeConfig[nodeName]}..."
  $EAP_HOME/bin/jboss-cli.sh --controller=localhost:$pamAdmPort --connect --commands="shutdown,exit"
  echo " DONE"
  rm -f ${tmpfile}*
}


function prepareConfigDB() {
  sqlite3 $CONFIG_DB "drop table if exists pamrc"
  sqlite3 $CONFIG_DB "create table pamrc (pkey integer primary key, installId varchar, pamInstall varchar, nodeName varchar, nodeBase varchar, nodeOffset varchar, nodePort varchar, controllerUrl varchar, installDir varchar, pamTarget varchar, startScript varchar, nodeCounter varchar)"
}

function nodeConfigSave() {
  local sql=""
  local pi=""
  for pi in ${nodeConfig['pamInstall']}; do
    sql="insert or ignore into pamrc (installId,pamInstall,nodeName,nodeBase,nodeOffset,nodePort,controllerUrl,installDir,pamTarget,startScript,nodeCounter) values ("
    sql="${sql}'$INSTALL_ID','$pi','${nodeConfig[nodeName]}','${nodeConfig[nodeBase]}','${nodeConfig[nodeOffset]}','${nodeConfig[nodePort]}','${nodeConfig[controllerUrl]}','$INSTALL_DIR','$target','${nodeConfig[startScript]}','${nodeConfig[nodeCounter]}')"
    sqlite3 $CONFIG_DB "${sql};"
  done
}

#
# - end of function definitions
#

INSTALL_ID=`randomid`

pamTargets=`extractHeaders`
TARGET_CONFIG=target.conf
goon=no
for target in $pamTargets; do
  #
  # initialize config vars
  #
  EAP7_ZIP=
  EAP_PATCH_ZIP=
  PAM_ZIP=
  KIE_ZIP=
  PAM_PATCH_ZIP=
  INSTALL_DIR=
  extractConfiguration $TARGET_CONFIG $target
  . $TARGET_CONFIG
  [[ -r "$EAP7_ZIP" ]] && [[ -r "$PAM_ZIP" ]] && [[ -r "$KIE_ZIP" ]] && [[ -r "$KIE_ZIP" ]] && goon=yes && break;
done

if [[ "$goon" == "yes" ]]; then
  sout "PROCEEDING WITH ${bold}${yellow}$target${normal} "
else
  echo "NO BINARIES FOR PAM INSTALLATION FOUND - ABORTING"
  exit 1
fi

rm -f $MASTER_CONFIG $TARGET_CONFIG

#
# - try to detect CYGWIN
#
CYGWIN_ON=no
# a=`uname -a` && al=${a,,} && ac=${al%cygwin} && [[ "$al" != "$ac" ]] && CYGWIN_ON=yes
# use awk to workaround MacOS bash version
a=`uname -a` && al=`echo $a | awk '{ print tolower($0); }'` && ac=${al%cygwin} && [[ "$al" != "$ac" ]] && CYGWIN_ON=yes
if [[ "$CYGWIN_ON" == "yes" ]]; then
  sout "CYGWIN DETECTED - WILL TRY TO ADJUST PATHS"
fi

nodeIP=''
optB=''
optC=''
optS=''
optO=''
multiNode=1
while getopts ":n:b:c:s:o:h" option; do
  case $option in
  	n ) nodeIP=$OPTARG;;
    b ) optB=$OPTARG;;
    c ) optC=$OPTARG;;
    s ) optS=$OPTARG;;
    o ) optO=$OPTARG;;
  	h ) usage; exit 1;;
  	: ) echo "No argument given"; opt=1; exit 1;;
  	* ) echo "Unknown option"; usage; opt=1; exit 1;;
  esac
done

prepareConfigDB

summary "--- $target Installation Summary ---" " "

# - check nodeIP values - default to localhost:8080
nodeIP=${nodeIP:-localhost:8080}
if [ -z $nodeIP ]; then
  echo "${bold}${red}*** ERROR ***${normal}: Option ${bold}${cyan}-n${normal} is mandatory"
  usage
  exit 1
fi
tmpfs=$IFS; IFS=':'; declare -a ar=($nodeIP); IFS=$tmpfs; nodeIP=${ar[0]}; nodePort=${ar[1]}; nodePort=':'${nodePort:-8080}; unset ar
BASE_URL='http://'${nodeIP}${nodePort}
basePort=${nodePort/:/} # this port will be used to autocalculate offset from 8080
# - check PAM installation value
pamValid=no
pamInstall=${optB:-both}
if [[ "$optB" =~ ^multi.* ]]; then
  pamInstall=multi
  declare -a multiArgs
  multiArgs=()
  while read -rd=; do multiArgs+=("$REPLY"); done <<<"${optB}="
  multiOption=${multiArgs[0]}
  multiNode=${multiArgs[1]}
  unset multiArgs multiOption
  [[ "$multiNode" -lt 2 ]] && echo "${bold}${red}*** ERROR ***${normal} : multi node cannot be less than 2 - ABORTING " && exit 1
fi
declare -a validPAMInstall=(kie controller both multi)
for i in "${validPAMInstall[@]}"; do
  if [ "$pamInstall" == "$i" ]; then
    pamValid=yes
  fi
done
if [ "$pamValid" != "yes" ]; then
  echo "${bold}${red}*** ERROR ***${normal} : Invalid mode for PAM installation [$pamInstall]"
  usage
  exit 1
else
  if [[ "$pamInstall" != "multi" ]]; then
    sout "PAM Installation mode : ${bold}${yellow}$pamInstall${normal}"
    summary "PAM Installation mode :- $pamInstall"
  else
    sout "PAM Installation mode : ${bold}${yellow}$pamInstall${normal} with ${bold}${yellow}$multiNode${normal} nodes"
    summary "PAM Installation mode :- $pamInstall with $multiNode nodes"
  fi
fi
# - check controller values for 'kie' PAM install mode
if [ "$pamInstall" != "kie" ]; then
  dv=${nodeIP}${nodePort}
fi
controllerList=${optC:-${dv}}
tmpfs=$IFS; IFS=','; declare -a ar=($controllerList); IFS=$tmpfs;
declare -a controllerListAr
for i in "${ar[@]}"; do
  controllerListAr=("${controllerListAr[@]}" http://${i})
done
unset ar
summary "Using Controller List :- ${controllerListAr[@]}" 
if [ "$pamInstall" == "kie" ] && [ ${#controllerListAr[@]} -lt 1 ]; then
  sout "ERROR: Controllers are madnatory for kie mode installation, none specified"
  usage
  exit 1
fi
smartRouter="$optS"
summary "Using Smart Router location :- ${smartRouter:-NOT INSTALLED}"
#
# - get extra options
#   option1=value1:option2=value2:
#
declare -A configOptions
if [[ ! -z "$optO" ]]; then
  declare -a multiOptions
  while read -rd:; do multiOptions+=("$REPLY"); done <<<"${optO}:"
  for ondx in ${!multiOptions[@]}; do
    while read -rd=; do tmpar+=("$REPLY"); done <<<"${multiOptions[$ondx]}="
    k="${tmpar[0]}"
    configOptions["$k"]="${tmpar[1]}"
    unset tmpar
  done
  unset tmpar multiOptions
fi
#
[[ ! -r $EAP7_ZIP ]]      && sout "ERROR: Cannot read EAP.7 ZIP file $EAP7_ZIP -- exiting"          && exit 1;
patchEAP=yes
[[ -z $EAP_PATCH_ZIP ]] && patchEAP=no
eap_patch_file_found=""
if [[ "$patchEAP" == "yes" ]]; then
  for eap_patch_file in `ls $EAP_PATCH_ZIP 2> /dev/null`; do
    [[ -r "$eap_patch_file" ]] && eap_patch_file_found="$eap_patch_file"
  done
fi
EAP_PATCH_ZIP="$eap_patch_file_found"
[[ -z $EAP_PATCH_ZIP ]] && patchEAP=no
[[ ! -z $EAP_PATCH_ZIP ]] && [[ ! -r $EAP_PATCH_ZIP ]] && sout "WARNING: Cannot read EAP PATCH ZIP file $EAP_PATCH_ZIP -- will proceed without it" && patchEAP=no

installPAM=yes
[[ ! -r $PAM_ZIP ]] && sout "WARNING: Cannot read PAM ZIP file $PAM_ZIP -- will proceed without it" && installPAM=no && patchPAM=no

skip_install=no && [[ -d $INSTALL_DIR ]] && sout "INFO: Installation detected at $INSTALL_DIR -- skipping installation" && skip_install=yes

cwd=$PWD

EAP_HOME=$cwd/$INSTALL_DIR

if [ "$skip_install" != "yes" ]; then
  #
  sout "Installing EAP at $EAP_HOME using $EAP7_ZIP"
  unzip -qq $EAP7_ZIP
  summary "Installed EAP using :- $EAP7_ZIP" 
  summary "EAP install location :- $INSTALL_DIR" 
  #
  cd $EAP_HOME
  if [[ "$patchEAP" == "yes" ]]; then
    sout "Patching EAP with $EAP_PATCH_ZIP"
    WORKDIR=$cwd
    [[ "$CYGWIN_ON" == "yes" ]] && WORKDIR=$(cygpath -w ${cwd})
    bin/jboss-cli.sh --output-json "patch apply $WORKDIR/$EAP_PATCH_ZIP"
    summary "Patched EAP with :- `bigString $EAP_PATCH_ZIP`" 
    pushd $EAP_HOME/bin &> /dev/null
      ./add-user.sh -s --user "$eapAdminName" --password "$eapAdminPasswd"
    popd &> /dev/null
  fi
  cd $cwd
  #
  # - deploy sample application to test session replication
  #   use
  #     http://load-balancer-ip/SessionCounter/Counter
  #   to get a session-based counter
  #   experiment by shutting down and restarting nodes while requesting
  #   for counter
  #
  war=SessionCounter.war && [[ -r $war ]] && cp $war $EAP_HOME/standalone/deployments
  #
  # - if multiNode make suitable number of copies
  #
  if [[ "$multiNode" -gt 1 ]]; then
    pushd $EAP_HOME &> /dev/null
      for node in `seq 2 $multiNode`; do
          cp -a standalone node${node}
      done
    popd &> /dev/null
  fi
  #
  cd $cwd
  if [[ "$installPAM" == "yes" ]]; then
    save_pamInstall=''
    for node in `seq 1 $multiNode`; do
      declare -A nodeConfig
      save_pamInstall=$pamInstall
      [[ "$node" -eq 1 ]] && [[ "$pamInstall" == "multi" ]] && pamInstall=both
      [[ "$node" -gt 1 ]] && [[ "$pamInstall" == "multi" ]] && pamInstall=kie && nodeParam=node${node}
      nodeConfig['pamInstall']=" "
      nodeParam=${nodeParam:-standalone}
      nodeConfig['nodeName']=${nodeParam}_`randomid`
      nodeConfig['nodeBase']=$nodeParam
      nodeConfig['nodeCounter']=node$node
      nodeCounter=$((node-1)) && [[ "$nodeParam" == "standalone" ]] && nodeCounter=0
      nodePort=$((basePort+nodeCounter*100))
      nodeOffset=$((nodePort-8080))
      nodeConfig['nodeOffset']=$nodeOffset 
      nodeConfig['nodePort']=$nodePort 
      sout "Installing ${bold}${blue}node${node}${normal} as ${bold}${blue}${nodeConfig[nodeName]}${normal}"
      summary "--- Node instalation node${node} as $nodeParam : ${nodeConfig[nodeName]}"
      ( [[ "$pamInstall" == "controller" ]] || [[ "$pamInstall" == "both" ]] ) && installBC && nodeConfig['pamInstall']="${nodeConfig['pamInstall']} controller"
      ( [[ "$pamInstall" == "kie" ]]        || [[ "$pamInstall" == "both" ]] ) && [[ -r $KIE_ZIP ]] && installKIE $nodeParam  && nodeConfig['pamInstall']="${nodeConfig['pamInstall']} kie"
      installUsers $nodeParam
      modifyConfiguration $nodeParam
      startUp $nodeParam $nodeCounter
      applyAdditionalNodeConfig $nodeParam
      # declare -p nodeConfig
      nodeConfigSave
      pamInstall=$save_pamInstall
      unset nodeConfig nodeCounter nodeOffset nodePort nodeParam node
    done
    unset save_pamInstall nodeParam component
  fi
  #
  # Configuring for post-commit hooks - relevant only for BC node
  #
  pushd $EAP_HOME &> /dev/null
    mkdir -p git-hooks-source
    mkdir -p git-hooks-messages
    cd git-hooks-messages
    : > Messages.properties
    for f in `seq 0 128`; do
      [[ $f -eq 0 ]] && echo "$f=Successfully commited to remote repository" >> Messages.properties
      [[ $f -ne 0 ]] && echo "$f=Error Code $f" >> Messages.properties
    done
    cp Messages.properties Messages_en.properties
    cd $EAP_HOME
    if [[ ! -r ../post-commit ]]; then
      cd git-hooks-source
      : > post-commit
      echo "#!/bin/sh" >> post-commit
      echo "git push origin +master" >> post-commit
      # echo "git push " >> post-commit
      chmod 744 post-commit
    else
      cp ../post-commit git-hooks-source
      chmod 744 git-hooks-source/post-commit
    fi
  popd &> /dev/null
  # print summary of installation
  summary " "
  prettyPrinter "${summaryAr[@]}"
fi  # - end of skip_install check


# - for debugging, enable command output, stop on first error
# set -x
# set -e

#
# - end of script
#
