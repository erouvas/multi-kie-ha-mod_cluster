#!/bin/bash

BASE_DIR=/vagrant

VB_PRIVATE_IP=$1
[[ -z "$VB_PRIVATE_IP" ]] && echo && echo && echo "ERROR: UNKNOWN VB_PRIVATE_IP - SET IT IN Vagrantfile --- ABORTING" && echo && echo && exit 10

#
# if core server exists, install this instead of the pre-packached Apache HTTPD
#
CORE_HTTP_SERVER=$BASE_DIR/jbcs-httpd24-httpd-2.4.37-RHEL7-x86_64.zip

#
# centos.7 - additional packages
#
yum -y install cairo-devel libjpeg-turbo-devel libpng-devel 
yum -y install ffmpeg-devel pango-devel libvncserver-devel pulseaudio-libs-devel libvorbis-devel libwebp-devel
yum -y install dejavu-sans-mono-fonts.noarch ant.noarch

yum -y install gcc java-1.8.0-openjdk java-1.8.0-openjdk-devel
yum -y install uuid-devel libssh2-devel openssl-devel openssl mod_proxy mod_ssl net-tools
yum -y install maven.noarch git-all.noarch unzip zip jq vim tcpdump
#
# required for "JBoss Core Services Apache HTTP Server on Red Hat Enterprise Linux"
# https://access.redhat.com/documentation/en-us/red_hat_jboss_core_services/2.4.23/html-single/apache_http_server_installation_guide/index#zip_installation
#
yum -y install apr apr-devel apr-util apr-util-devel apr-util-ldap elinks krb5-workstation mailcap

#
# configure SSHD to accept login by s/key
#
sed -i s/^ChallengeResponseAuthentication\ no/ChallengeResponseAuthentication\ yes/ /etc/ssh/sshd_config
systemctl restart sshd.service


if [[ ! -r $CORE_HTTP_SERVER ]]; then
  #
  # enable Apache HTTPD
  #
  systemctl enable httpd.service
  systemctl start httpd.service
else
  pushd /opt &> /dev/null
    unzip $CORE_HTTP_SERVER
    chown -R apache:apache jbcs-httpd24-2.4
    cd /opt/jbcs-httpd24-2.4/httpd
    ./.postinstall
    ./.postinstall.systemd
    # listen to all IPs
    cd conf
    
    # sed -i 's/\(^Listen\ \)127.*:\(80\)/\1\2/' httpd.conf
    sed -i '/^Listen\ 127/a Listen 81\nListen 82' httpd.conf
    echo '<VirtualHost *:81>'                                                               >> httpd.conf
    echo '  ProxyPass / balancer://aus/ stickysession=JSESSIONID|jsessionid nofailover=on'  >> httpd.conf
    echo '</VirtualHost>'                                                                   >> httpd.conf
    echo ' '                                                                                >> httpd.conf
    echo '<VirtualHost *:82>'                                                               >> httpd.conf
    echo '  ProxyPass / balancer://bra/ stickysession=JSESSIONID|jsessionid nofailover=on'  >> httpd.conf
    echo '</VirtualHost>'                                                                   >> httpd.conf
    
    cd ../conf.d
    mv mod_cluster.conf ../mod_cluster.conf.save
    : > mod_cluster.conf
    echo '# mod_proxy_balancer should be disabled when mod_cluster is used' >> mod_cluster.conf
    echo 'LoadModule proxy_cluster_module modules/mod_proxy_cluster.so'     >> mod_cluster.conf
    echo 'LoadModule cluster_slotmem_module modules/mod_cluster_slotmem.so' >> mod_cluster.conf
    echo 'LoadModule manager_module modules/mod_manager.so'                 >> mod_cluster.conf
    echo '# LoadModule advertise_module modules/mod_advertise.so'           >> mod_cluster.conf
    echo ' '                                                                >> mod_cluster.conf
    echo 'PersistSlots On'                                                  >> mod_cluster.conf
    echo 'AllowDisplay On'                                                  >> mod_cluster.conf
    echo 'CreateBalancers 1'                                                >> mod_cluster.conf
    echo 'MemManagerFile /opt/jbcs-httpd24-2.4/httpd/cache/mod_cluster'     >> mod_cluster.conf
    echo ' '                                                                >> mod_cluster.conf
    echo '<IfModule manager_module>'                                        >> mod_cluster.conf
    echo ' '                                                                >> mod_cluster.conf
    echo '  Listen 6666'                                                    >> mod_cluster.conf
    echo '  <VirtualHost *:6666>'                                           >> mod_cluster.conf
    echo '    ManagerBalancerName aus'                                      >> mod_cluster.conf
    echo '    DirectoryIndex disabled'                                      >> mod_cluster.conf
    echo '    <Directory />'                                                >> mod_cluster.conf
    echo '      Require ip 127.0.0.1'                                       >> mod_cluster.conf
    echo '    </Directory>'                                                 >> mod_cluster.conf
    echo '    EnableMCPMReceive'                                            >> mod_cluster.conf
    echo '    <Location /mod_cluster_manager>'                              >> mod_cluster.conf
    echo '      SetHandler mod_cluster-manager'                             >> mod_cluster.conf
    echo '      Require ip 127.0.0.1'                                       >> mod_cluster.conf
    echo '   </Location>'                                                   >> mod_cluster.conf
    echo '  </VirtualHost>'                                                 >> mod_cluster.conf
    echo ' '                                                                >> mod_cluster.conf
    echo '  Listen 6667'                                                    >> mod_cluster.conf
    echo '  <VirtualHost *:6667>'                                           >> mod_cluster.conf
    echo '    ManagerBalancerName bra'                                      >> mod_cluster.conf
    echo '    DirectoryIndex disabled'                                      >> mod_cluster.conf
    echo '    <Directory />'                                                >> mod_cluster.conf
    echo '      Require ip 127.0.0.1'                                       >> mod_cluster.conf
    echo '    </Directory>'                                                 >> mod_cluster.conf
    echo '    EnableMCPMReceive'                                            >> mod_cluster.conf
    echo '    <Location /mod_cluster_manager>'                              >> mod_cluster.conf
    echo '      SetHandler mod_cluster-manager'                             >> mod_cluster.conf
    echo '      Require ip 127.0.0.1'                                       >> mod_cluster.conf
    echo '   </Location>'                                                   >> mod_cluster.conf
    echo '  </VirtualHost>'                                                 >> mod_cluster.conf
    echo ' '                                                                >> mod_cluster.conf
    echo '</IfModule> '                                                     >> mod_cluster.conf
    chown apache:apache mod_cluster.conf

    # enable and start service
    systemctl start jbcs-httpd24-httpd.service
    systemctl enable jbcs-httpd24-httpd.service
  popd &> /dev/null
fi

#
# final touches
#
echo >> /root/.bashrc
echo "alias l='ls -alF'" >> /root/.bashrc
echo "alias ..='cd ..'" >> /root/.bashrc
echo "alias ...='cd ../..'" >> /root/.bashrc

#
# - goon with nexus if file is there
#
echo Resuming from `pwd`
ls -alF
[[ -r $BASE_DIR/bootstrap-nexus.sh ]] && cp $BASE_DIR/bootstrap-nexus.sh /root && cd /root && bash bootstrap-nexus.sh $VB_PRIVATE_IP
[[ -r $BASE_DIR/bootstrap-pam-both.sh ]] && cp $BASE_DIR/bootstrap-pam-both.sh /root && cd /root && bash bootstrap-pam-both.sh $VB_PRIVATE_IP
