#!/usr/bin/env jjs
#
# - enable scripting mode
#

#
# - configuration variables
#

#
# - configure application details
#
serverId='australia-kie';
containerName='geo_australia';
GAV_Group='com.example.simple_rule';
GAV_Artifact='SimpleRules';
GAV_Version='3-20200118004';

#
# - configure required capabilities with true or false
#
require_rules=true
require_bpmn=false
require_planner=false

#
# - configure BPMS installation details
#
baseURL='http://localhost:8080';
bpmsAdminName='pamAdmin';
bpmsAdminPasswd='S3cr3tK3y#';

#
# - configure verbosity
#     true : will print all messages
#    false : will print only errors, defaults to false
#
verbose=true

#
# - do not modify below this line
#

function read(inputStream){
    var inReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
    var inputLine;
    var response = new java.lang.StringBuffer();

    while ((inputLine = inReader.readLine()) != null) {
           response.append(inputLine);
    }
    inReader.close();
    return response.toString();
}

function write(outputStream, data){
    var wr = new java.io.DataOutputStream(outputStream);
    wr.writeBytes(data);
    wr.flush();
    wr.close();
}

function asResponse(con){
    var d = '';
    try {
      d = read(con.inputStream);
    } catch (e) {
      // NOP
    }

    return {data : d, statusCode : con.responseCode};
}

function httpPutWithHeaders(theUrl,requestProperties,putData) {
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "PUT";
    for (prop in requestProperties) {
      con.setRequestProperty(prop, requestProperties[prop]);
    }
    con.doOutput=true;
    write(con.outputStream, putData);

    return asResponse(con);
}
function httpGetWithHeaders(theUrl,requestProperties) {
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "GET";
    for (prop in requestProperties) {
      con.setRequestProperty(prop, requestProperties[prop]);
    }

    return asResponse(con);
}
function httpDeleteWithHeaders(theUrl,requestProperties) {
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "DELETE";
    for (prop in requestProperties) {
      con.setRequestProperty(prop, requestProperties[prop]);
    }

    return asResponse(con);
}
function httpGet(theUrl){
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "GET";

    return asResponse(con);
}

function httpPost(theUrl, data, requestProperties){
    var con = new java.net.URL(theUrl).openConnection();

    con.requestMethod = "POST";
    for (prop in requestProperties) {
      con.setRequestProperty(prop, requestProperties[prop]);
    }

    // Send post request
    con.doOutput=true;
    write(con.outputStream, data);

    return asResponse(con);
}

if (verbose!=true) verbose=false;
function logit(s,err) { if (verbose||err) print(s); }
function pout(s) { logit(s,false); }
function eout(s) { logit(s,true); }
function sout(s) { pout(''); pout(s); }
function PASS(s) { pout("Test PASSED"+(s?" : "+s:"")); }
function FAIL(s) { eout("Test FAILED"+(s?" : "+s:"")); END_RUN(2); }
function END_RUN(exitCode) { logit('--- END-RUN',exitCode); exit((exitCode?exitCode:0)); }

pout("");
pout("--- BEGIN");

invokedOK=true
invokedOK=(invokedOK && (baseURL.length()>0))
invokedOK=(invokedOK && (bpmsAdminName.length()>0))
invokedOK=(invokedOK && (bpmsAdminPasswd.length()>0))
invokedOK=(invokedOK && (containerName.length()>0))
invokedOK=(invokedOK && (GAV_Group.length()>0))
invokedOK=(invokedOK && (GAV_Artifact.length()>0))
invokedOK=(invokedOK && (GAV_Version.length()>0))
if (!invokedOK) {
  eout('ERROR: Mising configuration, please provide values for the following:');
  eout('               baseURL: the URL where the Business Central is reachable,');
  eout('                        eg. http://localhost:8080/business-central');
  eout('         bpmsAdminName: the name of the BPMS administrator, e.g. bpmsAmdin');
  eout('       bpmsAdminPasswd: the password of the BPMS administrator, e.g. s3cr3tp4ss');
  eout('             GAV_Group: the GROUP part of the Maven GAV vector, e.g. com.example.rules');
  eout('          GAV_Artifact: the ARTIFACT part of the Maven GAV vector, e.g. validation');
  eout('           GAV_Version: the VERSION part of the Maven GAV vector, e.g. 1.0-SNAPSHOT or 2.5.1');
  END_RUN(1);
}

bpmsAuth='Basic '+java.util.Base64.getEncoder().encodeToString((bpmsAdminName+':'+bpmsAdminPasswd).getBytes('utf-8'));

eapOK=false;
bpmsOK=false;

scCode = httpGet(baseURL).statusCode;
if (scCode==200) { PASS("EAP is available at "+baseURL); eapOK=true; } else FAIL('EAP is unreachable');

if (eapOK) {
  scCode = httpGet("${baseURL}/business-central").statusCode;
  if (scCode==200) { PASS('BPMS / Business-Central is reachable'); bpmsOK=true; } else FAIL('BPMS is unreachable');
}

if (bpmsOK) {
  sout('Checking KIE Execution Server accessibility...');
  propConfig = { };
  propConfig['Authorization'] = bpmsAuth;
  response = httpGetWithHeaders("${baseURL}/kie-server/services/rest/server",propConfig);
  if (response.statusCode==200) {
    PASS();
    //print(response.data);
    var is = new org.xml.sax.InputSource();
    is.setCharacterStream(new java.io.StringReader(response.data));
    var dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
    var db = dbf.newDocumentBuilder();
    var doc = db.parse(is);
    var nodes = doc.getElementsByTagName("response");
    var el = nodes.item(0);
    pout('Response is : '+el.getAttribute('type'));
    nodes = doc.getElementsByTagName("content");
    var el = nodes.item(0);
    pout('KIE Server Info :'+el.getFirstChild().getData());

    sout("Cheking KIE Execution Server capabilities...");
    var has_kieServer=false;
    var has_rules=false;
    var has_bpmn=false;
    var has_bpmnUI=false;
    var has_planner=false;
    var has_required_capabilities=false;
    nodes = doc.getElementsByTagName("capabilities");
    for (var i=0; i<nodes.length; i++) {
      if (nodes.item(i).getFirstChild().getData()=='KieServer') has_kieServer=true;
      if (nodes.item(i).getFirstChild().getData()=='BRM') has_rules=true;
      if (nodes.item(i).getFirstChild().getData()=='BPM') has_bpmn=true;
      if (nodes.item(i).getFirstChild().getData()=='BPM-UI') has_bpmnUI=true;
      if (nodes.item(i).getFirstChild().getData()=='BRP') has_planner=true;
    }
    has_required_capabilities=( has_kieServer && (require_rules?has_rules:true) && (require_bpmn?has_bpmn:true) && (require_planner?has_planner:true) );
    if (!has_required_capabilities) {
      eout('KIE Execution Services '+(has_kieServer?'ARE':'ARE NOT')+' activated'+', Required: YES');
      eout('Business Rules Services '+(has_rules?'ARE':'ARE NOT')+' activated'+', Required: '+(require_rules?'YES':'NO'));
      eout('Business Process Execution Services '+(has_bpmn?'ARE':'ARE NOT')+' activated'+', Required: '+(require_rules?'YES':'NO'));
      eout('Business Process Design Services '+(has_bpmnUI?'ARE':'ARE NOT')+' activated'+', Required: '+(require_rules?'YES':'NO'));
      eout('Business Resource Planner Services '+(has_planner?'ARE':'ARE NOT')+' activated'+', Required: '+(require_rules?'YES':'NO'));
      FAIL('KIE Execution Server required capabilities NOT satisfied');
    }
  } else {
    FAIL('KIE Execution Server is unreachable');
  }
 
  sout('Testing available KIE Execution Servers for this controller...');
  propConfig = { };
  propConfig = { 'Accept':'application/json', 'Content-Type':'application/json' };
  propConfig['Authorization'] = bpmsAuth;
  response = httpGetWithHeaders("${baseURL}/business-central/rest/controller/management/servers",propConfig);
  if (response.statusCode==200) {
    PASS();
    jon = JSON.parse(response.data);
    //print(JSON.stringify(jon,null,'\t'));
    var srvId = jon['server-template'][0]['server-id'];
    var srvName = jon['server-template'][0]['server-name'];
    var srvTemplate =jon['server-template'][0]['server-instances'][0]['server-template-id']; 
    var srvUrl =jon['server-template'][0]['server-instances'][0]['server-url']; 
    pout('Server Details:');
    pout('\t         ID:'+srvId);
    pout('\t       Name:'+srvName);
    pout('\t TemplateID:'+srvTemplate);
    pout('\t        URL:'+srvUrl);
  } else {
    FAIL('No KIE Execution Server found on this controller');
  }
  srvId=serverId

  sout("Deleting container ${containerName} on the Controller");
  propConfig = { };
  propConfig = { 'Accept':'application/json', 'Content-Type':'application/json' };
  propConfig['Authorization'] = bpmsAuth; 
  response = httpDeleteWithHeaders("${baseURL}/business-central/rest/controller/management/servers/${srvId}/containers/${containerName}",propConfig);
  pout('ResponseCode: ['+response.statusCode+']');

  sout("Trying to creating container ${containerName} at Business Central ...");
  var putData = [
                  '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>',
                  '<container-spec-details>',
                  '	<container-id>'+"${containerName}"+'</container-id>',
                  '	<container-name>'+"${containerName}"+'</container-name>',
                  '	<server-template-key>',
                  '		<server-id>'+"${srvId}"+'</server-id>',
                  '	</server-template-key>',
                  '	<release-id>',
                  "		<group-id>${GAV_Group}</group-id>",
                  "		<artifact-id>${GAV_Artifact}</artifact-id>",
                  "		<version>${GAV_Version}</version>",
                  '	</release-id>',
                  '	<configs>',
                  '		<entry>',
                  '			<key>PROCESS</key>',
                  '			<value xsi:type="processConfig"',
                  '				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
                  '             <runtimeStrategy>SINGLETON</runtimeStrategy>',
                  '             <kbase></kbase>',
                  '             <ksession></ksession>',
                  '             <mergeMode>MERGE_COLLECTIONS</mergeMode>',
                  '			</value>',
                  '		</entry>',
                  '		<entry>',
                  '			<key>RULE</key>',
                  '			<value xsi:type="ruleConfig"',
                  '				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
                  '				<scannerStatus>STOPPED</scannerStatus>',
                  '			</value>',
                  '		</entry>',
                  '	</configs>',
                  '	<status>STARTED</status>',
                  '</container-spec-details>'].join('');
  propConfig = { };
  propConfig = { 'Accept':'application/json', 'Content-Type':'application/xml' };
  propConfig['Authorization'] = bpmsAuth;
  response = httpPutWithHeaders("${baseURL}/business-central/rest/controller/management/servers/${srvId}/containers/${containerName}",propConfig,putData);
  if (response.statusCode==201) {
    PASS();
    sout("Associating with remote server ... ");
    var putData = [
                    '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>',
                    '<kie-container container-id="'+"${containerName}"+'">',
                    '  <release-id>',
                    "    <group-id>${GAV_Group}</group-id>",
                    "    <artifact-id>${GAV_Artifact}</artifact-id>",
                    "    <version>${GAV_Version}</version>",
                    '  </release-id>',
                    '</kie-container>'].join('');
    propConfig = { };
    propConfig = { 'Accept':'application/json', 'Content-Type':'application/xml' };
    propConfig['Authorization'] = bpmsAuth;
    //response = httpPutWithHeaders("${srvUrl}/containers/${containerName}",propConfig,putData);
    //pout('Response code is:'+response.statusCode);
    //if (response.statusCode==201) {
    //  PASS();
    //  var is = new org.xml.sax.InputSource();
    //  is.setCharacterStream(new java.io.StringReader(response.data));
    //  var dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();
    //  var db = dbf.newDocumentBuilder();
    //  var doc = db.parse(is);
    //  var nodes = doc.getElementsByTagName("response");
    //  var el = nodes.item(0);
    //  pout('Response is : '+el.getAttribute('type')+' '+el.getAttribute('msg'));
    //} else {
    //  FAIL("ERR10 Could NOT create ${containerName} updated with ${GAV_Group}:${GAV_Artifact}:${GAV_Version}");
    //}
  } else {
    FAIL("ERR11 Could NOT create ${containerName} updated with ${GAV_Group}:${GAV_Artifact}:${GAV_Version}");
  }


 }

eout("${containerName} updated with ${GAV_Group}:${GAV_Artifact}:${GAV_Version}");
END_RUN();

