# Load Balancing KIE Containers deployed in multiple KIE Servers with mod_cluster

## Description
mod_cluster is often used to provide load balancing and HA between KIE Containers deployed in KIE Servers usually in cases were KIE Servers hosts identical KIE Containers

However when deploying different KIE Containers to different KIE Servers (or to KIE Server Groups) a regular mod_cluster installation does not cope well since the load balancing context is based on the initial part of KIE Containers URL; which in this case is always "/kie-server". Hence mod_cluster is unable to route requests to the KIE Server that hosts a particular KIE Container.

This project presents a mod_cluster deployment that is able to handle exactly that.

Two KIE Server Groups, "australia" and "brazil", are created each one with two KIE Servers. On each KIE Server group a different KIE Container, "geo_australia" and "geo_brazil" respectively, is deployed. The deployment is done through a single Business Central instance that acts as a controller for all KIE Severs.

Next mod_cluster is configured to load balance deployed KIE Containers based on the KIE Server Group that's hosting them.
The deployment is highlighted in the following figure.

![ ](images/ha_for_multi_kie_deployments.png)


## SSL configuration

It is often desirable to expose services through a single SSL-backed port instead of different ports per service. HTTPS can be added on top of the configuration presented with the following modfications to the `/opt/jbcs-httpd24-2.4/httpd/conf.d/ssl.conf` file.

First enable the SSL port (443) by allowing the HTTPD server to bind to the 443 port as follows:

```
# Listen localhost:443
Listen *:443
```

Then in the VirtualHost configured in the `ssl.conf` file, add the following lines:

```
ProxyRequests Off

ProxyPass        /aus            http://127.0.0.1:81/
ProxyPassReverse /aus            http://127.0.0.1:81/
ProxyPassReverseCookiePath /aus  http://127.0.0.1:81/

ProxyPass        /bra            http://127.0.0.1:82/
ProxyPassReverse /bra            http://127.0.0.1:82/
ProxyPassReverseCookiePath /bra  http://127.0.0.1:82/

ProxyPreserveHost On
```
and restart the server.

With these modifications to the configuration all HTTPS requests (port 443) which contain a URL prefixed with `/aus` will be redircted to port 81 and then to the load balancer configured for handling the "australia" workload.

Similarly requests with the `/bra` prefix will be redirected to port 82 and to the load balancer for handling the "brazil" workload.

An an example the following scripts can be used to demonstrate invoking KIE Containers through the HTTPS port.
- `simple-rule/invoke-australia-https.sh` to invoke the "australia" workload
- `simple-rule/invoke-brazil-https.sh` to invoke the "brazil" workload

## Usage
Use "vagrant up" to start the installation. The same commend can subsequently used to start an halted box. After some time a Vagrant box named "multiKIE" will be created.

All components of RHPAM will be automatically started during system start up as well as the Nexus RPM and Apache HTTPD with mod_cluster.

When the Vagrant box has started (and installation completed) login to the box and invoke "australia" and "brazil" KIE Containers with the following:

| Action | command |
|-|-|
| 1. login   | `vagrant ssh`| 
| 2. switch to root | `su -` (password:vagrant is the default) |
| 3. go to simple_rule mini project | `cd simple-rule` |
| 4. invoke australia KIE Server group | `./invoke-australia.sh` |
| 5. invoke brazil KIE Server group | `./invoke-brazil.sh` |
| 6. invoke australia KIE Server group through the HTTPS port (if configured) | `./invoke-australia-https.sh` |
| 7. invoke brazil KIE Server group through the HTTPS port (if configured) | `./invoke-brazil-https.sh` |

Rules firing will also produced output on the server log of each KIE Server. Check the following logs to monitor rule execution

| Server Component | log location |
|-|-|
| KIE Server one for australia | `/opt/jboss/standalone/log/server.log`|
| KIE Server two for australia | `/opt/jboss/node2/log/server.log`|
| KIE Server one for brazil | `/opt/jboss/node3/log/server.log`|
| KIE Server two for brazil | `/opt/jboss/node4/log/server.log`|
| Business Central | `/opt/jboss/standalone/log/server.log`|
| HTTPD / mod_cluster | `/opt/jbcs-httpd24/httpd/log/{access,error}.log`|

The following ports will be used:

|Component  | 192.168.20.20:Port |
|-|-|
|Nexus | `:8081` |
|Business Central | `:8080` |
| KIE Server one for australia | `:8080` |
| KIE Server two for australia | `:8180` |
| KIE Server one for brazil | `:8280` |
| KIE Server two for brazil | `:8380` |
| load balancing port for australia | `:81`|
| load balancing port for brazil | `:82`|
| mod_cluster_manager for australia | `:6666` |
| mod_cluster_manager for brazil | `:6667` |
| Apache HTTPD | `:80` |
| Apache HTTPD HTTPS (if configured) | `:443` |


## Installation Details
Everything will be deployed in a Vagrant box using the virtualbox engine. The following components will be deployed:
* CentOS.7
* Java.8, maven and git
* RHPAM.7.4.1
* JBoss EAP.7.2.0 and patch 7.2.4
* Nexus Repository Manager
* Sample Rules project to be deployed
* Sample "curl"-based script to invoke the deployed KIE Containers

The Vagrant box has been configured with:
* 8GB of memory
* private networking with the IP "192.168.20.20"

***IMPORTANT: The following binaries should be provided and must reside in the home directory of this project***

|Component  | Files |
|-|-|
|RHPAM | `rhpam-7.4.1-business-central-eap7-deployable.zip` |
|      | `rhpam-7.4.0-kie-server-ee8.zip`  |
|JBoss EAP | `jboss-eap-7.2.0.zip`, `jboss-eap-7.2.2-patch.zip` |
| Apache HTTPD with mod_cluster | `jbcs-httpd24-httpd-2.4.37-RHEL7-x86_64.zip` |

The rest of the required binaries will be downloaded as part of the setup procedure.


> More about Vagrant commands : [Vagrant Cheat Sheet](https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4)
> Written with [StackEdit](https://stackedit.io/).
