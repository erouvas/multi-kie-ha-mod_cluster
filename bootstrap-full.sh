#!/bin/bash

BASE_DIR=/vagrant

#
# - config vars
#
WGET_CMD='wget --no-check-certificate -q '

#
# disable SELinux
#
setenforce 0
cp /etc/sysconfig/selinux /etc/sysconfig/selinux.orig
cat << __SELNX_OFF > /etc/sysconfig/selinux
SELINUX=disabled
SELINUXTYPE=targeted
__SELNX_OFF

yum install -y wget
# yum -y update
$WGET_CMD "http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm"
yum -y install epel-release-latest-7.noarch.rpm
#
# centos.7 - additional packages 
#
yum -y install cairo-devel libjpeg-turbo-devel libpng-devel
yum -y install ffmpeg-devel pango-devel libvncserver-devel pulseaudio-libs-devel libvorbis-devel libwebp-devel
yum -y install dejavu-sans-mono-fonts.noarch ant.noarch 

yum -y install gcc java-1.8.0-openjdk java-1.8.0-openjdk-devel 
yum -y install uuid-devel libssh2-devel openssl-devel openssl mod_proxy mod_ssl net-tools
yum -y install maven.noarch git-all.noarch unzip zip jq

#
# configure SSHD to accept login by s/key
#
sed -i s/^ChallengeResponseAuthentication\ no/ChallengeResponseAuthentication\ yes/ /etc/ssh/sshd_config
systemctl restart sshd.service

#
# enable Apache HTTPD
#
systemctl enable httpd.service
systemctl start httpd.service

#
# configure maven settings.xml
#
cp $BASE_DIR/settings.xml /etc/maven
chmod a+r /etc/maven/settings.xml

#
# install EAP and PAM
#
for f in $BASE_DIR/*; do
  ln -s $f
done
$BASE_DIR/pam-setup.sh -b both -n localhost:8080

#
# systemd integration
#
$BASE_DIR/pam7-systemd-integration.sh

#
# Continue with RHPAM and BC setup
#
counter=0
goon=no
while [[ "$goon" == "no" ]]; do
  let counter=$((counter+1))
  if [[ ! -r /opt/jboss/standalone/deployments/business-central.war.deployed ]]; then
    echo "[ $counter ] Waiting for PAM to be fully deployed... will check again in 5 seconds..."
    sleep 5s
  else
    goon=yes
  fi
done

echo "PAM ${PAM_VERSION} along with Nexus Repository Manager have been installed"
echo
echo "Use http://localhost:8080/business-central   for PAM"
echo "    http://localhost:8081                    for Nexus"
echo
